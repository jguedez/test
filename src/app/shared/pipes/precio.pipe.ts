import { Pipe, PipeTransform } from '@angular/core';

// Le ponemos un nombre a la tuberia
@Pipe({name: 'precio'})

// Definimos la clase implementado la interface PipeTransform
export class PrecioPipe implements PipeTransform {

 // La pipe recibirá el 2 parámetros 
 transform(value: any): any {
    
    let resul= parseInt(value)/Math.pow(10,6);
    
    if(resul>=1){
    return "BsF. "+resul+" Millones"
    }
    return "BsF. "+"0";

}

 
}