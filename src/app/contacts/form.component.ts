import { Input, EventEmitter , Component, OnInit, NgZone } from '@angular/core';
import { ProductsService } from '../shared/services/products.service';
import { ContactsService } from '../shared/services/contacts.service';
import { UsersService } from '../shared/services/users.service';
import { CommercialOfficeService } from '../shared/services/commercialOffices.service';
import { AuthService } from '../shared/services/auth.service';


@Component({
    selector: 'contact-form',
    templateUrl: './form.template.html'
})

export class ContactForm implements OnInit{

    @Input() contact;
    @Input() loaded : EventEmitter<any>;
    public current_lookup_enable = false;
    public current_lookup_data = [];
    public current_lookup_selected;
    public current_lookup_callback;
    public current_lookup_fields = [];
    public user;
    
    public contact_types = [
        {label: '', value: ""},
        //{label: 'Administración SKY', value: "Administración SKY"},
        //{label: 'Agente Junior', value: "Agente Junior"},
        {label: 'Comprador', value: "Comprador"},
        {label: 'Corredor Inmobiliario-Gremio', value: "Corredor Inmobiliario-Gremio"},
        //{label: 'Equipo Sky', value: "Equipo Sky"},
        //{label: 'Franquicia Comercial', value: "Franquicia Comercial"},
        //{label: 'Franquicia Personal', value: "Franquicia Personal"},
        {label: 'Inquilino', value: "Inquilino"},
        {label: 'Invitado Foro', value: "Invitado Foro"},
        {label: 'Preventa', value: "Preventa"},
        {label: 'Propietario', value: "Propietario"},
        {label: 'Prospecto de Cliente', value: "Prospecto de Cliente"},
        {label: 'Prospecto de Franquicia', value: "Prospecto de Franquicia"},
        //{label: 'Proveedor', value: "Proveedor"},
        //{label: 'Retirado de SKY', value: "Retirado de SKY"},
        //{label: 'Socio', value: "Socio"},
    ];

    public contact_sources = [
        {label: '', value: ""},
        {label: 'Alianza Inmobiliaria', value: "Alianza Inmobiliaria"},
        {label: 'Asesor o Agente', value: "Asesor o Agente"},
        {label: 'Conlallave.com', value: "Conlallave.com'"},
        {label: 'Correo Electronico', value: "Correo Electronico"},
        {label: "Expo CIMA", value: "Expo CIMA"},
        {label: "Facebook", value: "Facebook"},
        {label: "Formulario de Consulta Sobre Franquicias", value: "Formulario de Consulta Sobre Franquicias"},
        {label: "Formulario de Registro a Evento",value: "Formulario de Registro a Evento"},
        {label: "Instagram", value: "Instagram"},
        {label: "Llamada Telefónica", value: "Llamada Telefónica"},
        {label: "Llamada Durante Guardia", value: "Llamada Telefónica"},
        {label: "Portales Gratuitos", value: "Portales Gratuitos"},
        {label: "Tuinmueble.com", value: "Tuinmueble.com"},
        {label: "Twitter", value: "Twitter"},
        {label: "Web Sky Group", value: "Web Sky Group"},
        {label: "Formulario de Registro", value: "Formulario de Registro"},
        {label: "Google+", value: "Google+"},
    ];

    public interest_sources = [
        {label: '', value: ""},        
        {label: 'Conlallave.com', value: "Conlallave.com"},
        {label: 'TUinmueble.com', value: "TUinmueble.com"},        
        {label: 'Amigo o Conocido', value: "Amigo o Conocido"},        
        {label: 'Banner', value: "Banner"},        
        {label: 'Charla Informativa', value: "Charla Informativa"},        
        {label: 'Colega Inmobilario', value: "Colega Inmobilario"},        
        {label: 'Correo Electrónico', value: "Correo Electrónico"},        
        {label: 'Facebook', value: "Facebook"},        
        {label: 'Google', value: "Google"},        
        {label: 'Otro', value: "Otro"},        
        {label: 'Pagina Web SkyGroup', value: "Pagina Web SkyGroup"},        
        {label: 'Periodico', value: "Periodico"},        
        {label: 'Periodico o Revista', value: "Periodico o Revista"},        
        {label: 'Promoción Asesor SKY', value: "Promoción Asesor SKY"},        
        {label: 'Promoción Asesor SKY', value: "Promoción Asesor SKY"},        
        {label: 'Twitter', value: "Twitter"},        
        {label: 'Ya es Cliente', value: "Ya es Cliente"},        
        {label: 'Youtube', value: "Youtube"},        
        {label: 'Pendón', value: "Pendón"},        
        {label: 'Portal Inmobiliario Gratuito', value: "Portal Inmobiliario Gratuito"},        
        {label: 'Instagram', value: "Instagram"},        
    ];

    public yes_no_options = [
        {label: "", value: "" },
        {label: "SI", value: "SI" },
        {label: "NO", value: "NO" }
    ];

    public commercial_offices = [];

    constructor (private productsService: ProductsService, private usersService: UsersService, private commercialOfficeService: CommercialOfficeService, private authService: AuthService) {
        this.user = this.authService.getLoggedUser();
    }

    ngOnInit() {
        this.loaded.subscribe(contact => {
            if(!contact.id) {
                this.contact.agent_id   = this.user.contact_id;
                this.contact.agent_name = this.user.name + " " + this.user.last_name;
            }
        });

       this.commercialOfficeService.all({selected_fields: "id,name"}).subscribe( offices => {
            var offices = offices.response.map(office => {
                return {
                    "label": office.name,
                    "value": office.id
                };
            });
            offices.unshift({"label": "","value": ""})
            this.commercial_offices = offices;
        });

       
       
    }

    

       /* INTERESTED PRODUCT LOOK UP */

    interestedProductLookUp() {
        this.current_lookup_enable = true;
        this.productsService.all({selected_fields: "id,name,code"}).subscribe( products => {
            this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "code", label: "Código"}
            ];
            this.current_lookup_callback = this.interestedOnSelect;
            this.current_lookup_data = products.response;
        });
    }

    interestedOnSelect() {
        this.contact.interested_product = this.current_lookup_selected.id;
        this.contact.interested_product_name = this.current_lookup_selected.name;
        this.turnOffLookUp();
    }

    interestedOnClear() {
        delete this.contact.interested_product;
        delete this.contact.interested_product_name;
    }

    /* END  INTERESTED PRODUCT LOOK UP */

    usersLookUp(callback) {
        this.current_lookup_enable = true;
        this.usersService.all({selected_fields: "id,contact_id,name,last_name,email"}).subscribe( users => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = callback;
            this.current_lookup_data = users.response;
        });
    }

    /* AGENT LOOK UP */
    agentLookUp() {
        this.usersLookUp(this.agentOnSelect);
    }

    agentOnSelect() {
        this.contact.agent_id = this.current_lookup_selected.contact_id;
        this.contact.agent_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    agnetOnClear() {
        delete this.contact.agent_id;
        delete this.contact.agent_name;
    }

    /* END AGENT LOOK UP */


    /* ASSIGNED TO LOOK UP */
    assignedLookUp() {
        this.usersLookUp(this.assignedOnSelect);
    }

    assignedOnSelect() {
        this.contact.assigned_id = this.current_lookup_selected.contact_id;
        this.contact.assigned_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    assignedOnClear() {
        delete this.contact.assigned_id;
        delete this.contact.assigned_name;
    }
    /* END ASSIGNED LOOK UP */

    turnOffLookUp() {
        this.current_lookup_enable = false;
        this.current_lookup_fields = [];
        this.current_lookup_data = [];
        this.current_lookup_selected = false;
        this.current_lookup_callback = false;
    }
    
}