import { Component, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { VisitsService } from "../shared/services/visits.service";
import { GridComponent, GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';
import { parseDate } from '@telerik/kendo-intl';

@Component({
    selector: 'visits',
    templateUrl: './visits.template.html'
})
export class VisitPage implements OnDestroy, OnInit {
    public visits = [];
    public loadingEnable = true;
    public filter;
    public gridData: GridDataResult;
    public state = {
        skip: 0,
        take: 50
    };
    public date = new Date();

    protected dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridData = process(this.visits, this.state);
    }


    constructor(private visitsService: VisitsService, private router: Router, private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.disableLoading();
        console.log(this.date);
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.filter = params['filter'];
            this.loadingEnable = true;
            this.visits = [];
            this.getVisits(1);

        });

        console.log(this.filter);

    }

    ngOnDestroy() {
        this.loadingEnable = false;
    }

    onSelect(event) {
        this.disableLoading();
        this.router.navigate(['/app/activity/edit/' + event.id]);
    }

    disableLoading() {
        this.loadingEnable = false;
    }

    getVisits(page) {
        this.visitsService.all({ page: page, filter: this.filter }).subscribe(visits => {

            if (this.filter == 'toDo') {
                page == 1 ? this.visits = [] : null;

                visits.response.data.forEach(record => {
                                        
                    record.to = record.to ? new Date(record.to) : null;

                    if (this.date < record.to) {
                        
                        this.visits = this.visits.concat(record);
                    }
                });

                this.gridData = process(this.visits, this.state);

                if (visits.response.data.length > 0 && this.loadingEnable === true) { this.getVisits(page + 1) }
            } else {


                this.visits = visits.response.data ?
                    this.visits.concat(visits.response.data) : this.visits;



                this.gridData = process(this.visits, this.state);

                if (visits.response.data.length > 0 && this.loadingEnable === true) {
                    this.getVisits(page + 1)
                }
            }});
    }
}
