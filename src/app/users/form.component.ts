import { Component, OnInit, NgZone } from '@angular/core';
import { Input } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../shared/services/users.service';
import { ContactsService } from '../shared/services/contacts.service';

declare var swal: any;

@Component({
    selector: 'user-form',
    templateUrl: './form.template.html'
})

export class UserForm {

    @Input() user;
    public current_lookup_enable = false;
    public current_lookup_data = [];
    public current_lookup_selected;
    public current_lookup_callback;
    public current_lookup_fields = [];

    public levels = [
        { label: "Administrador", value: "administrator" },
        { label: "fraquicia", value: "franchise" },
        { label: "agente", value: "agent" },        
    ]

    constructor (private usersService: UsersService, private contactsService: ContactsService, private router: Router) {    }

    /* CONTACT LOOK UP */
     contactLookUp() {
        this.current_lookup_enable = true;
        this.contactsService.all({selected_fields: "id,name,last_name,email"}).subscribe( contacts => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = this.contactOnSelect;
            this.current_lookup_data = contacts.response;
        });
    }

    contactOnSelect() {
        this.user.contact_id = this.current_lookup_selected.id;
        this.user.contact_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    contactOnClear() {
        delete this.user.contact_id;
        delete this.user.contact_name;
    }
    /* END CONTACT LOOK UP */

     /* USER LOOK UP */
     userLookUp() {
        this.current_lookup_enable = true;
        this.usersService.all({selected_fields: "id,name,last_name"}).subscribe( users => {
            this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"}
            ];
            this.current_lookup_callback = this.userOnSelect;
            this.current_lookup_data = users.response;
        });
    }

    userOnSelect() {
        this.user.parent_user_id = this.current_lookup_selected.id;
        this.user.parent_user_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    userOnClear() {
        delete this.user.parent_user_id;
        delete this.user.parent_user_name;
    }
    /* END USER LOOK UP */

    turnOffLookUp() {
        this.current_lookup_enable = false;
        this.current_lookup_fields = [];
        this.current_lookup_data = [];
        this.current_lookup_selected = false;
        this.current_lookup_callback = false;
    }

}