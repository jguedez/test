import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service'
import { Observable, AsyncSubject } from 'rxjs/Rx';
import { DatePipe } from '@angular/common';


@Injectable()
export class VisitsService {

    constructor(private apiService: ApiService, private datePipe: DatePipe) { }

    type(body = {}) {
        return this.apiService.call('activity/type', 'GET', body);
    }

    all(body = {}) {
        return this.apiService.call('activity', 'GET', body);
    }

    findById(id) {
        return this.apiService.call('activity/' + id, 'GET');
    }

    create(visit) {
        return this.apiService.call('activity', 'POST', this.parseDates(visit));
    }

    update(visit) {
        return this.apiService.call('activity/' + visit.id, 'PUT', this.parseDates(visit));
    }

    parseDates(visit) {
        var parsed = {};
        for (var field in visit) { parsed[field] = visit[field]; }
        parsed['subject'] = visit.subject;
        parsed['from'] = this.datePipe.transform(visit.from, 'y-MM-dd hh:mm:ss');
        parsed['to'] = this.datePipe.transform(visit.to, 'y-MM-dd hh:mm:ss');        
        return parsed;
    }
}
