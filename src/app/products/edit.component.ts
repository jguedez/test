import { Component, OnInit, NgZone, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { Http, Headers } from "@angular/http";
import { md5 } from "../md5";
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../shared/services/products.service';

declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html'
})
export class EditProduct implements OnInit {

    public product = { links: {}};
    public processing = false;
    public productEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private productsService: ProductsService) {
        this.route.params.subscribe(params => {
            this.productsService.findById(params['id']).subscribe(product => {
                product                       = product.response;
                product.environments          = product.environments          ? product.environments.split(";")          : [];
                product.building_environments = product.building_environments ? product.building_environments.split(";") : [];
                product.facilities            = product.facilities            ? product.facilities.split(";")            : [];
                product.services              = product.services              ? product.services.split(";")              : [];
                product.offer_type            = product.offer_type            ? product.offer_type.split(";")            : [];
                product.additional_details    = product.additional_details    ? product.additional_details.split(";")    : [];
                this.product                  = product;
                this.productEventEmitter.emit(product);
            });
        });
    }

    ngOnInit() { }

    private save() {
        this.processing = true;
        this.productsService.update(this.product).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Producto editado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/products']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}