import { Component, OnInit, NgZone, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { Http, Headers } from "@angular/http";
import { md5 } from "../md5";
import { ActivatedRoute, Router } from '@angular/router';
import { ContactsService } from '../shared/services/contacts.service';

declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html'
})
export class EditContact implements OnInit {

    public contact = {};
    public processing = false;
    public contactEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private contactsService: ContactsService) {
        this.route.params.subscribe(params => {
            this.contactsService.findById(params['id']).subscribe(contact => {
                this.contact = contact.response;
                this.contactEventEmitter.emit(contact);
            });
        });
    }

    ngOnInit() { }

    private save() {
        this.processing = true;
        this.contactsService.update(this.contact).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Contacto editado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/contacts']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}