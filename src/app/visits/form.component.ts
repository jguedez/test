import { Input, EventEmitter , Component, OnInit, NgZone } from '@angular/core';
import { ContactsService } from '../shared/services/contacts.service';
import { VisitsService } from '../shared/services/visits.service';
import { ProductsService } from '../shared/services/products.service';
import { AuthService } from '../shared/services/auth.service';

@Component({
    selector: 'visit-form',
    templateUrl: './form.template.html'
})

export class VisitForm implements OnInit{

    @Input() visit;
    @Input() loaded : EventEmitter<any>;
    public current_lookup_enable = false;
    public current_lookup_data = [];
    public current_lookup_selected;
    public current_lookup_callback;
    public current_lookup_fields = [];
    public user;
    public activities;

    constructor (private contactsService: ContactsService, private visitsService: VisitsService, private authService: AuthService, private productsService: ProductsService) {
        this.user = this.authService.getLoggedUser();
    }

    ngOnInit() {
        this.loaded.subscribe(visit => {
            if(!visit.id) {
                this.visit.agent_id   = this.user.contact_id;
                this.visit.agent_name = this.user.name + " " + this.user.last_name;
                this.visit.from = new Date;
            }
        });

        this.getActivity();
    }

    getActivity() {        
        this.visitsService.type().subscribe(activity => 
        {
            this.activities = activity.response;            
        });
    }

    contactLookUp(callback) {
        this.current_lookup_enable = true;
        this.contactsService.all({selected_fields: "id,name,last_name,email"}).subscribe( contacts => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = callback;
            this.current_lookup_data = contacts.response;
        });
    }


    /* AGENT LOOK UP */
    agentLookUp() {
       this.contactLookUp(this.agentOnSelect);
    }

    agentOnSelect() {
        this.visit.agent_id   = this.current_lookup_selected.id;
        this.visit.agent_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    agnetOnClear() {
        delete this.visit.agent_id;
        delete this.visit.agent_name;
    }
    /* END AGENT LOOK UP */
    
    /* CLIENT LOOK UP */
    clientLookUp() {
        this.contactLookUp(this.clientOnSelect);
    }

    clientOnSelect() {
        this.visit.client_id = this.current_lookup_selected.id;
        this.visit.client_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    clientOnClear() {
        delete this.visit.agent_id;
        delete this.visit.agent_name;
    }
    /* END CLIENT LOOK UP */

    /* CLIENT LOOK UP */

    productLookUp() {
        this.current_lookup_enable = true;
        this.productsService.all({selected_fields: "id,name,bsf_price,usd_price,address"}).subscribe( contacts => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "bsf_price", label: "Precio en Bs"},
                {name: "usd_price", label: "Monto en US$"}, {name: "address", label: "Dirección"}, 
            ];
            this.current_lookup_callback = this.productOnSelect;
            this.current_lookup_data = contacts.response;
        });
    }

    productOnSelect() {
        this.visit.product_id = this.current_lookup_selected.id;
        this.visit.product_name = this.current_lookup_selected.name
        this.turnOffLookUp();
    }

    productOnClear() {
        delete this.visit.product_name;
        delete this.visit.product_id;
    }

    /* END PRODUCT LOOK UP */

    turnOffLookUp() {
        this.current_lookup_enable = false;
        this.current_lookup_fields = [];
        this.current_lookup_data = [];
        this.current_lookup_selected = false;
        this.current_lookup_callback = false;
    }
    
}