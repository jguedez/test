import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service'
import { Observable, AsyncSubject } from 'rxjs/Rx';


@Injectable()
export class ContactsService {

    constructor(private apiService: ApiService) { }

    all(body = {}) {
        return this.apiService.call('contacts', 'GET', body);
    }

    findById(id) {
        return this.apiService.call('contacts/' + id, 'GET');
    }

    create(contact) {
        return this.apiService.call('contacts', 'POST', contact);
    }

    update(contact) {
        return this.apiService.call('contacts/' + contact.id, 'PUT', contact);
    }

    checkAvailability(contact) {
        return this.apiService.call('contacts/check_availability', 'POST', contact);
    }
}
