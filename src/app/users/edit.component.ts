import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { Http, Headers } from "@angular/http";
import { md5 } from "../md5";
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../shared/services/users.service';

declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html',
})

export class EditUser implements OnInit {

    public user = {};
    public processing = false;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private usersService: UsersService) {
        this.route.params.subscribe(params => {
            this.usersService.findById(params['id']).subscribe(user => {
                this.user = user.response;
            });
        });
    }

    ngOnInit() { }

    private save() {
        this.processing = true;
        this.usersService.update(this.user).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Usuario editado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/users']);
                    }, 2000);
                }, 500);
            }
        });
    }

    deleteUser(id) {
        var router = this.router;
        var userService = this.usersService;
        swal({
            title: "¿Está seguro?",
            text: "El usuario será eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(confirm){
            if (confirm) {
                userService.delete(id).subscribe(response => {
                    if (response.code == 1) {
                        setTimeout(function () {
                            swal.close();
                            router.navigate(['/app/users']);
                        }, 1000);
                    }
                });
            }
            else{ swal.close(); }
        });
    }

    private goBack() {
        this.location.back();
    }
}