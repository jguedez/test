import { Input, EventEmitter, Component, OnInit, NgZone } from '@angular/core';
import { ContactsService } from '../shared/services/contacts.service';
import { AuthService } from '../shared/services/auth.service';
import { UsersService } from '../shared/services/users.service';
import { CommercialOfficeService } from '../shared/services/commercialOffices.service';

@Component({
    selector: 'product-form',
    templateUrl: './form.template.html'
})

export class ProductForm {

    @Input() product;
    @Input() loaded : EventEmitter<any>;
    public current_lookup_enable = false;
    public current_lookup_data = [];
    public current_lookup_selected;
    public current_lookup_callback;
    public current_lookup_fields = [];
    public user;

    public environments = [
        {label: "", value: ""},
        {label: "Recepción", value: "Recepción"},
        {label: "Taquilla", value: "Taquilla"},
        {label: "Recepción", value: "Recepción"},
        {label: "Sala de espera", value: "Sala de espera"},
        {label: "Sala de reuniones", value: "Sala de reuniones"},
        {label: "Biblioteca", value: "Biblioteca"},
        {label: "Archivo", value: "Archivo"},
        {label: "Kitchenette", value: "Kitchenette"},
        {label: "Comedor", value: "Comedor"},
        {label: "Terraza", value: "Terraza"},
        {label: "Area secretaria", value: "Area secretaria"},
        {label: "Mantenimiento", value: "Mantenimiento"},
        {label: "Open space", value: "Open space"},
        {label: "Sin tabiquear", value: "Sin tabiquear"},
        {label: "Duplex", value: "Duplex"},
        {label: "Depositos", value: "Depositos"},
        {label: "Estac. techado", value: "Estac. techado"},
        {label: "Salón", value: "Salón"},
        {label: "Salón/comedor", value: "Salón/comedor"},
        {label: "Cocina emp", value: "Cocina emp"},
        {label: "Estudio", value: "Estudio"},
        {label: "Estar", value: "Estar"},
        {label: "Vestier", value: "Vestier"},
        {label: "Terraza cub", value: "Terraza cub"},
        {label: "Terraza desc", value: "Terraza desc"},
        {label: "Hab servicio", value: "Hab servicio"},
        {label: "Hab chofer", value: "Hab chofer"},
        {label: "Baño servicio", value: "Baño servicio"},
        {label: "Baño auxiliar", value: "Baño auxiliar"},
        {label: "Jardin", value: "Jardin"},
        {label: "Maletero", value: "Maletero"},
        {label: "Otro", value: "Otro"}
    ];

    public building_environments = [
        {label: "", value: ""},
        {label: "Area comercial", value: "Area comercial"},
        {label: "Ascensor", value: "Ascensor"},
        {label: "Ascensor de carga", value: "Ascensor de carga"},
        {label: "Ascensor privado", value: "Ascensor privado"},
        {label: "Auditorio", value: "Auditorio"},
        {label: "Calle privada", value: "Calle privada"},
        {label: "Conferencias", value: "Conferencias"},
        {label: "Estac. público", value: "Estac. público"},
        {label: "Exposiciones", value: "Exposiciones"},
        {label: "Gimnasio", value: "Gimnasio"},
        {label: "Jardin", value: "Jardin"},
        {label: "Otro", value: "Otro"},
        {label: "Parque", value: "Parque"},
        {label: "Parrillera", value: "Parrillera"},
        {label: "Piscina", value: "Piscina"},
        {label: "Recepción", value: "Recepción"},
        {label: "Salón", value: "Salón"},
        {label: "Sauna", value: "Sauna"},
        {label: "Tenis", value: "Tenis"},
        {label: "Vigilancia", value: "Vigilancia"},
        {label: "Zona de carga", value: "Zona de carga"}
    ];

    public facilities = [
        {label: "", value: ""},
        {label: "Aire acondicionado", value: "Aire acondicionado"},
        {label: "Alfombra", value: "Alfombra"},
        {label: "Amoblado", value: "Amoblado"},
        {label: "Cable", value: "Cable"},
        {label: "Cableado red", value: "Cableado red"},
        {label: "Central telefónica", value: "Central telefónica"},
        {label: "Cerámica", value: "Cerámica"},
        {label: "Equipado", value: "Equipado"},
        {label: "Jacuzzi", value: "Jacuzzi"},
        {label: "Gas directo", value: "Gas directo"},
        {label: "Líneas telefónicas", value: "Líneas telefónicas"},
        {label: "Otro", value: "Otro"},
        {label: "Parabólica", value: "Parabólica"},
        {label: "Parquet", value: "Parquet"},
        {label: "Planta Eléctrica", value: "Planta Eléctrica"},
        {label: "Semiamoblado", value: "Semiamoblado"},
        {label: "Sist. seguridad", value: "Sist. seguridad"},
        {label: "Tanque de Agua", value: "Tanque de Agua"}
    ];

    public yes_no_options = [
        {label: "", value: "" },
        {label: "SI", value: "SI" },
        {label: "NO", value: "NO" }
    ]

    public services = [
        {label: "", value: "" },
        {label: "Electricidad", value: "Electricidad" },
        {label: "Agua", value: "Agua" },
        {label: "Aguas servidas", value: "Aguas servidas" },
        {label: "Teléfono", value: "Teléfono" },
        {label: "Gas directo", value: "Gas directo" },
        {label: "Aseo urbano", value: "Aseo urbano" },
        {label: "Acceso de tierra", value: "Acceso de tierra" },
        {label: "Pavimento", value: "Pavimento" },
        {label: "Urbanizado", value: "Urbanizado" },
        {label: "Rural", value: "Rural" },
        {label: "Urbano", value: "Urbano" },
        {label: "Otro", value: "Otro" },
    ];

    public offer_type = [
        {label: "", value: "" },
        {label: "Venta", value: "Venta" },
        {label: "Alquiler", value: "Alquiler" },
        {label: "Preventa", value: "Preventa" },
        {label: "Teléfono", value: "Teléfono" },
        {label: "Alquiler o venta", value: "Alquiler o venta" },
        {label: "Alquiler con opción a compra", value: "Alquiler con opción a compra" },
    ];

    public additional_details = [
        {label: "", value: "" },
        {label: "Exoneración por registro de Vivienda Principal", value: "Exoneración por registro de Vivienda Principal" },
        {label: "Prepago de impuesto sobre la renta 0,5%", value: "Prepago de impuesto sobre la renta 0,5%" },
    ];

    public categories = [
        {label: "", value: "" },
        {label: "Apartamento", value: "Apartamento" },
        {label: "Aparto - Quinta", value: "Aparto - Quinta" },
        {label: "Casa", value: "Casa" },
        {label: "Edificio", value: "Edificio" },
        {label: "Finca", value: "Finca" },
        {label: "Fondo de Comercio", value: "Fondo de Comercio" },
        {label: "Galpón", value: "Galpón" },
        {label: "Local", value: "Local" },
        {label: "Ofi-Depósitos", value: "Ofi-Depósitos" },
        {label: "Oficina", value: "Oficina" },
        {label: "Residencial", value: "Residencial" },
        {label: "Servicios", value: "Servicios" },
        {label: "Terreno", value: "Terreno" },
        {label: "Town House", value: "Town House" },

    ];

    public commercial_offices = [];

    public countries = [
        {label: "", value: "" },
        {label: "VENEZUELA", value: "VENEZUELA" },
        {label: "ESPAÑA", value: "ESPAÑA" },
        {label: "E.E.U.U.", value: "E.E.U.U." },
        {label: "PANAMA", value: "PANAMA" },
    ];

    constructor (private contactsService: ContactsService, private authService: AuthService, private commercialOfficeService: CommercialOfficeService, private usersService: UsersService) {   
        this.user = this.authService.getLoggedUser();
    }


    ngOnInit() {
        this.loaded.subscribe(product => {
            if(!product.id) {
                this.product.captor_person_id   = this.user.contact_id;
                this.product.captor_person_name = this.user.name + " " + this.user.last_name;
            }
        });

        this.commercialOfficeService.all({selected_fields: "id,name"}).subscribe( offices => {
            var offices = offices.response.map(office => {
                return {
                    "label": office.name,
                    "value": office.id
                };
            });
            offices.unshift({"label": "","value": ""})
            this.commercial_offices = offices;
        });
    }

    openLink(url){
        if(url.length > 1) {
            window.open(url);
        }
    }

    usersLookUp(callback) {
        this.current_lookup_enable = true;
        this.usersService.all({selected_fields: "id,contact_id,name,last_name,email"}).subscribe( users => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = callback;
            this.current_lookup_data = users.response;
        });
    }


    /* OWNER LOOK UP */
    onwerLookUp() {
        this.current_lookup_enable = true;
        this.contactsService.all({selected_fields: "id,name,last_name,email"}).subscribe( contacts => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = this.ownerOnSelect;
            this.current_lookup_data = contacts.response;
        });
    }

    ownerOnSelect() {
        this.product.owner_id = this.current_lookup_selected.id;
        this.product.owner_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    ownerOnClear() {
        delete this.product.owner_id;
        delete this.product.owner_name;
    }
    /* END OWNER LOOK UP */
    


    /* CAPTOR PERSON LOOK UP */
    captorPersonLookUp() {
        this.usersLookUp(this.captorPersonOnSelect);
    }

    captorPersonOnSelect() {
        this.product.captor_person_id = this.current_lookup_selected.contact_id;
        this.product.captor_person_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    captorPersonOnClear() {
        delete this.product.captor_person_id;
        delete this.product.captor_person_name;
    }
    /* END CAPTOR PERSON LOOK UP */

    /* SALES PERSON LOOK UP */
    salesPersonLookUp() {
        this.usersLookUp(this.salesPersonOnSelect);
    }

    salesPersonOnSelect() {
        this.product.sales_person_id = this.current_lookup_selected.contact_id;
        this.product.sales_person_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    salesPersonOnClear() {
        delete this.product.sales_person_id;
        delete this.product.sales_person_name;
    }
    /* END SALES PERSON LOOK UP */

    /* ASSIGNED TO LOOK UP */
    assignedLookUp() {
        this.usersLookUp(this.assignedOnSelect);
    }

    assignedOnSelect() {
        this.product.assigned_id = this.current_lookup_selected.contact_id;
        this.product.assigned_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    assignedOnClear() {
        delete this.product.assigned_id;
        delete this.product.assigned_name;
    }
    /* END ASSIGNED LOOK UP */

    turnOffLookUp() {
        this.current_lookup_enable = false;
        this.current_lookup_fields = [];
        this.current_lookup_data = [];
        this.current_lookup_selected = false;
        this.current_lookup_callback = false;
    }
}