import { Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';

// Guards
import { GuestGuard } from 'app/shared/guards/guest.guard';
import { AuthGuard } from 'app/shared/guards/auth.guard';

export const ROUTES: Routes = [{
   path: '', redirectTo: 'app', pathMatch: 'full'
  },
  {
    path: 'app',  canActivate: [AuthGuard],  loadChildren: './layout/layout.module#LayoutModule'
  },
  {
    path: 'login',  canActivate: [GuestGuard], loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'error', component: ErrorComponent
  },
  {
    path: '**',    component: ErrorComponent
  }
];
