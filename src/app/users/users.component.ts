import { Component, OnDestroy, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from "../shared/services/users.service";
import { GridComponent, GridDataResult, DataStateChangeEvent} from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';

@Component({
	selector: 'users',
	templateUrl: './users.template.html'
})
export class UsersPage implements OnDestroy {
    private users = [];
    private loadingEnable = true;

    public gridData: GridDataResult;
    public state = {
        skip: 0,
        take: 50
    };

    protected dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridData = process(this.users, this.state);
    }

	constructor(private usersService: UsersService, private router: Router) {
        this.getUsers(1);
        this.loadingEnable = true;
	}

	ngOnDestroy() { 
        this.loadingEnable = false;
    }

    onSelect(event) {
        this.disableLoading();
        this.router.navigate(['/app/users/edit/' + event.id]);
    }

    private disableLoading() {
        this.loadingEnable = false;
    }


    getUsers(page) {
        this.usersService.all({page: page}).subscribe(users => 
        {
            page == 1 ? this.users = [] : null;

            this.users = users.response.data ? 
                                this.users.concat(users.response.data) : this.users;

            this.gridData = process(this.users, this.state);

            if(users.response.data.length > 0 && this.loadingEnable === true) { this.getUsers( page + 1) }
        });
    }
}
