import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Http, Headers } from "@angular/http";
import { AuthService } from '../shared/services/auth.service';
import { Utilities } from '../shared/utilities/utilities';

@Component({
  selector: 'login',
  styleUrls: [ './login.style.scss' ],
  templateUrl: './login.template.html',
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'login-page app'
  }
})

export class Login implements OnInit {
    public email: string;
    public password: string;
    public auth_error = false;
    public processing = false;

    constructor(private authService: AuthService, private router: Router) { }

    ngOnInit() {
        Utilities.scrollTop();
    }

    login() {
        this.auth_error = false;
        this.processing = true;
        this.authService.login(this.email, this.password)
            .subscribe( response => {
                if (response['response'] && response['code'] == 1) {
                    this.setAuthStorage(response['response']);
                    this.router.navigate(['app/dashboard']);
                }
                else { this.auth_error = true; }
                this.processing = false;
            });
    }

    setAuthStorage(response) {
        localStorage.setItem('token', response.token);
        localStorage.setItem('user', JSON.stringify(response.user));
        console.log(response.user);
    }

    logout() {
        this.authService.logout();
        this.router.navigate(['/']);
    }
}

