import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ContactsPage } from './contacts.component';
import { CreateContact } from './new.component';
import { EditContact } from './edit.component';
import { SearchPipe } from './search.pipe';
import { ContactForm } from './form.component';
import { DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule } from 'primeng/primeng';
import { GridModule } from '@progress/kendo-angular-grid';


export const routes = [
	{ path: '', component: ContactsPage, pathMatch: 'full' },
	{ path: 'create', component: CreateContact, pathMatch: 'full' },
	{ path: 'edit/:id', component: EditContact, pathMatch: 'full' },
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(routes), FormsModule, 
                DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, GridModule],
	declarations: [ContactsPage, CreateContact, EditContact, SearchPipe, ContactForm]
})
export class ContactsModule {
	static routes = routes;
}
