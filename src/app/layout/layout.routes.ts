import { Routes, RouterModule } from '@angular/router';
import { Layout } from './layout.component';
import { AdminGuard } from '../shared/guards/admin.guard';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
	{
		path: '', component: Layout, children: [
			{ path: '', redirectTo: 'dashboard', pathMatch: 'full' },
			{ path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
			{ path: 'contacts', loadChildren: '../contacts/contacts.module#ContactsModule' },
			{ path: 'products', loadChildren: '../products/products.module#ProductsModule' },
			{ path: 'activity', loadChildren: '../visits/visits.module#VisitModule' },
			{ path: 'properties_registration', loadChildren: '../properties_registration/properties_registration.module#PropertiesRegistrationModule' },
			{ path: 'users', canActivate: [AdminGuard], loadChildren: '../users/users.module#UsersModule' },
		]
	}
];

export const ROUTES = RouterModule.forChild(routes);
