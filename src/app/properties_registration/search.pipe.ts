import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchPipe'
})
export class SearchPipe implements PipeTransform {
    transform(value, args: string): Array<any> {
        if (args == "" || !args) return value;
        if (value) {
            return value.filter(el => this.contained(el, args));
        }
    }
    private contained(el, args) {
        for (var prop in el)
            if (el[prop] != null && el[prop].toLowerCase().indexOf(args.toLowerCase()) > -1)
                return true;
        return false;
    }
}