import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UsersPage } from './users.component';
import { CreateUser } from './new.component';
import { EditUser } from './edit.component';
import { UserForm } from './form.component';
import { SearchPipe } from './search.pipe';

import { DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, MultiSelectModule } from 'primeng/primeng';
import { GridModule } from '@progress/kendo-angular-grid';


export const routes = [
	{ path: '', component: UsersPage, pathMatch: 'full' },
	{ path: 'create', component: CreateUser, pathMatch: 'full' },
	{ path: 'edit/:id', component: EditUser, pathMatch: 'full' },
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(routes), FormsModule, 
    MultiSelectModule,DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, GridModule],
	declarations: [UsersPage, CreateUser, EditUser, UserForm, SearchPipe]
})
export class UsersModule {
	static routes = routes;
}
