import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PropertiesRegistrationPage } from './properties_registration.component';
import { CreatePropertyRegistration } from './new.component';
import { EditPropertyRegistration } from './edit.component';
import { SearchPipe } from './search.pipe';
import { PropertyRegistrationForm } from './form.component';
import { DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, MultiSelectModule } from 'primeng/primeng';
import { GridModule } from '@progress/kendo-angular-grid';


export const routes = [
	{ path: '', component: PropertiesRegistrationPage, pathMatch: 'full' },
	{ path: 'create', component: CreatePropertyRegistration, pathMatch: 'full' },
	{ path: 'edit/:id', component: EditPropertyRegistration, pathMatch: 'full' },
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(routes), FormsModule, 
                DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, MultiSelectModule, GridModule],
	declarations: [PropertiesRegistrationPage, CreatePropertyRegistration, EditPropertyRegistration, SearchPipe, PropertyRegistrationForm]
})
export class PropertiesRegistrationModule {
	static routes = routes;
}
