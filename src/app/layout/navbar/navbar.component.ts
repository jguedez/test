import { Component, EventEmitter, OnInit, ElementRef, Output } from '@angular/core';
import { AppConfig } from '../../app.config';
import { AuthService } from '../../shared/services/auth.service';
declare let jQuery: any;

@Component({
	selector: '[navbar]',
	templateUrl: './navbar.template.html'
})
export class Navbar implements OnInit {
	@Output() toggleSidebarEvent: EventEmitter<any> = new EventEmitter();
	@Output() toggleChatEvent: EventEmitter<any> = new EventEmitter();
	$el: any;
	config: any;
	public user: any;

	constructor(el: ElementRef, config: AppConfig, private authService: AuthService) {
		this.$el = jQuery(el.nativeElement);
		this.config = config.getConfig();
        this.user = this.authService.getLoggedUser();
	}

	toggleSidebar(state): void {
		this.toggleSidebarEvent.emit(state);
	}

	logOut() {
		this.authService.logout()
	}


	ngOnInit(): void {
		setTimeout(() => {
			let $chatNotification = jQuery('#chat-notification');
			$chatNotification.removeClass('hide').addClass('animated fadeIn')
				.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', () => {
					$chatNotification.removeClass('animated fadeIn');
					setTimeout(() => {
						$chatNotification.addClass('animated fadeOut')
							.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd' +
							' oanimationend animationend', () => {
								$chatNotification.addClass('hide');
							});
					}, 8000);
				});
			$chatNotification.siblings('#toggle-chat')
				.append('<i class="chat-notification-sing animated bounceIn"></i>');
		}, 4000);

		this.$el.find('.input-group-addon + .form-control').on('blur focus', function (e): void {
			jQuery(this).parents('.input-group')
			[e.type === 'focus' ? 'addClass' : 'removeClass']('focus');
		});
	}
}
