import { Component, OnInit } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UsersService } from '../shared/services/users.service';

declare var swal: any;

@Component({
    selector: 'create',
    templateUrl: './new.template.html'
})
export class CreateUser implements OnInit {

    public user = {};
    public processing = false;

    constructor(private router: Router, private location: Location, private usersService: UsersService) { }

    ngOnInit() { }

    private create() {
        this.processing = true;
        this.usersService.create(this.user).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Usuario creado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/users']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}