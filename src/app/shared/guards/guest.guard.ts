import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Rx';

@Injectable()

export class GuestGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var logged = this.authService.isLoggedIn();
        if(!logged){
            return Observable.of(true);
        }
        this.router.navigate(['/app/dashboard']);
        return Observable.of(false);
    }
}