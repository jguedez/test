import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service'
import { Observable, AsyncSubject } from 'rxjs/Rx';


@Injectable()
export class CommercialOfficeService {

    constructor(private apiService: ApiService) { }

    all(body = {}) {
        return this.apiService.call('commercial_offices', 'GET', body);
    }
}
