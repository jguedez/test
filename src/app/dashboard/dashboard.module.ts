import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { RouterModule } from '@angular/router';
import { Dashboard } from './dashboard.component.ts';
import { Widget } from '../layout/widget/widget.directive';
import { DataTableModule } from 'primeng/primeng';
import { ChartsModule, SparklineModule, ChartModule } from '@progress/kendo-angular-charts';
export const routes = [
  { path: '', component: Dashboard, pathMatch: 'full' }
];


@NgModule({
  imports: [ CommonModule, RouterModule.forChild(routes), ChartModule, DataTableModule, ChartsModule ],
  declarations: [ Dashboard, Widget ]
})
export class DashboardModule {
  static routes = routes;
}
