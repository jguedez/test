import { Input, EventEmitter , Component, OnInit, NgZone } from '@angular/core';
import { ContactsService } from '../shared/services/contacts.service';
import { PropertiesRegistrationService } from '../shared/services/properties_registration.service';
import { ProductsService } from '../shared/services/products.service';
import { AuthService } from '../shared/services/auth.service';
import { CommercialOfficeService } from '../shared/services/commercialOffices.service';
import { UsersService } from '../shared/services/users.service';


@Component({
    selector: 'pr-form',
    templateUrl: './form.template.html'
})

export class PropertyRegistrationForm implements OnInit{

    @Input() PR;
    @Input() loaded : EventEmitter<any>;
    public current_lookup_enable = false;
    public current_lookup_data = [];
    public current_lookup_selected;
    public current_lookup_callback;
    public current_lookup_fields = [];
    public user;
    public commercial_offices;

    public types = [
        {label: "", value: ""},
        {label: "Venta", value: "Venta"},
        {label: "Alquiler", value: "Alquiler"},
        {label: "Otro", value: "Otro"},       
    ];

    public statuses = [
        {label: "", value: ""},
        {label: "Reserva", value: "Reserva"},
        {label: "Promesa bilateral", value: "Promesa bilateral"},
        {label: "Protocolización", value: "Protocolización"},
    ];

    public royalties = [
        {label: "", value: ""},
        {label: "Master Internacional", value: "Master Internacional"},
        {label: "Nacional", value: "Nacionall"},
    ];

    public lawyer_requirements = [
        {label: "Doc.  de Propiedad del  Inmueble", value: "Doc.  de Propiedad del  Inmueble"},
        {label: "Fotocopia de la C.I. Propietario (s)", value: "Fotocopia de la C.I. Propietario (s)"},
        {label: "Fotocopia de la C.I. compradores (s)", value: "Fotocopia de la C.I. compradores (s)"},
        {label: "Fotocopia del Cheque de Venta", value: "Fotocopia del Cheque de Venta"},
        {label: "Cédula Catastral (original)", value: "Cédula Catastral (original)"},
        {label: "Solvencias (expedida por la alcaldía)", value: "Solvencias (expedida por la alcaldía)"},
        {label: "Transacción Inmobiliaria (expedida por la alcaldía)", value: "Transacción Inmobiliaria (expedida por la alcaldía)"},
        {label: "Forma 33 Seniat (si aplica)", value: "Forma 33 Seniat (si aplica)"},
        {label: "Vivienda Principal (si aplica)", value: "Vivienda Principal (si aplica)"}
    ];

    constructor (private contactsService: ContactsService, private PRService: PropertiesRegistrationService, 
    private authService: AuthService, private productsService: ProductsService, private commercialOfficeService: CommercialOfficeService, private usersService: UsersService) {
        this.user = this.authService.getLoggedUser();
    }

    ngOnInit() {
        this.loaded.subscribe(visit => {
            if(!visit.id) {
                this.PR.agent_id   = this.user.contact_id;
                this.PR.agent_name = this.user.name + " " + this.user.last_name;
                this.PR.agent_id   = this.user.contact_id;
                this.PR.agent_name = this.user.name + " " + this.user.last_name;

                this.PR.from = new Date;
            }
        });

        this.commercialOfficeService.all({selected_fields: "id,name"}).subscribe( offices => {
            var offices = offices.response.map(office => {
                return {
                    "label": office.name,
                    "value": office.id
                };
            });
            offices.unshift({"label": "","value": ""})
            this.commercial_offices = offices;
        });
    }

    openLink(url){
        if(url.length > 1) {
            window.open(url);
        }
    }

    contactLookUp(callback) {
        this.current_lookup_enable = true;
        this.contactsService.all({selected_fields: "id,name,last_name,email"}).subscribe( contacts => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = callback;
            this.current_lookup_data = contacts.response;
        });
    }

    usersLookUp(callback) {
        this.current_lookup_enable = true;
        this.usersService.all({selected_fields: "id,contact_id,name,last_name,email"}).subscribe( users => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "last_name", label: "Apellido"},{name: "email", label: "Correo"}
            ];
            this.current_lookup_callback = callback;
            this.current_lookup_data = users.response;
        });
    }


    /* CAPTOR PERSON LOOK UP */
    captorPersonLookUp() {
        this.usersLookUp(this.captorPersonOnSelect);
    }

    captorPersonOnSelect() {
        this.PR.captor_person_id = this.current_lookup_selected.contact_id;
        this.PR.captor_person_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    captorPersonOnClear() {
        delete this.PR.captor_person_id;
        delete this.PR.captor_person_name;
    }
    /* END CAPTOR PERSON LOOK UP */

    /* LINK LOOK UP */
    linkLookUp() {
        this.contactLookUp(this.linkOnSelect);
    }

    linkOnSelect() {
        this.PR.link_person_id = this.current_lookup_selected.id;
        this.PR.link_person_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    linkOnClear() {
        delete this.PR.link_id;
        delete this.PR.link_name;
    }
    /* END LINK LOOK UP */
    

    /* SALES PERSON LOOK UP */
    salesPersonLookUp() {
        this.usersLookUp(this.salesPersonOnSelect);
    }

    salesPersonOnSelect() {
        this.PR.sales_person_id = this.current_lookup_selected.contact_id;
        this.PR.sales_person_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    salesPersonOnClear() {
        delete this.PR.sales_person_id;
        delete this.PR.sales_person_name;
    }
    /* END SALES PERSON LOOK UP */
    
    /* CLIENT LOOK UP */
    clientLookUp() {
        this.contactLookUp(this.clientOnSelect);
    }

    clientOnSelect() {
        this.PR.contact_id = this.current_lookup_selected.id;
        this.PR.contact_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    clientOnClear() {
        delete this.PR.contact_name;
        delete this.PR.contact_id;
    }
    /* END CLIENT LOOK UP */

    /* SELLER CLIENT LOOK UP */
    sellerClientLookUp() {
        this.contactLookUp(this.sellerClientOnSelect);
    }

    sellerClientOnSelect() {
        this.PR.seller_contact_id = this.current_lookup_selected.id;
        this.PR.seller_contact_name = this.current_lookup_selected.name + " " + this.current_lookup_selected.last_name;
        this.turnOffLookUp();
    }

    sellerClientOnClear() {
        delete this.PR.seller_contact_name;
        delete this.PR.seller_contact_id;
    }
    /* END CLIENT LOOK UP */

    /* PRODUCT LOOK UP */

    productLookUp() {
        this.current_lookup_enable = true;
        this.productsService.all({selected_fields: "id,name,bsf_price,usd_price,address"}).subscribe( contacts => {
           this.current_lookup_fields = [
                {name: "name", label: "Nombre"}, {name: "bsf_price", label: "Precio en Bs"},
                {name: "usd_price", label: "Monto en US$"}, {name: "address", label: "Dirección"}, 
            ];
            this.current_lookup_callback = this.productOnSelect;
            this.current_lookup_data = contacts.response;
        });
    }

    productOnSelect() {
        this.PR.product_id = this.current_lookup_selected.id;
        this.PR.product_name = this.current_lookup_selected.name
        this.turnOffLookUp();
    }

    productOnClear() {
        delete this.PR.product_name;
        delete this.PR.product_id;
    }

    /* END PRODUCT LOOK UP */

    turnOffLookUp() {
        this.current_lookup_enable = false;
        this.current_lookup_fields = [];
        this.current_lookup_data = [];
        this.current_lookup_selected = false;
        this.current_lookup_callback = false;
    }
    
}