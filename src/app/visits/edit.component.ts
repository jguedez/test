import { Component, OnInit, NgZone, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { Http, Headers } from "@angular/http";
import { md5 } from "../md5";
import { ActivatedRoute, Router } from '@angular/router';
import { VisitsService } from '../shared/services/visits.service';

declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html'
})
export class EditVisit implements OnInit {

    public visit = {};
    public processing = false;
    public visitEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private visitsService: VisitsService) {
        this.route.params.subscribe(params => {
            this.visitsService.findById(params['id']).subscribe(visit => {
                this.visit = visit.response;
                this.visit['to'] = new Date(this.visit['to']);
                this.visit['from'] = new Date(this.visit['from']);
                this.visitEventEmitter.emit(visit);
            });
        });
    }

    ngOnInit() { }

    private save() {
        this.processing = true;
        this.visitsService.update(this.visit).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Visita editada",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/activity']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}