import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http";
import { Router } from "@angular/router";
import { ApiService } from "../shared/services/api.service";
import { AuthService } from "../shared/services/auth.service";

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.template.html'
})
export class Dashboard implements OnInit {
	constructor(private router: Router, private apiService: ApiService, private authService: AuthService) { }

    public lastest_products = [];
    public visits_graph_data;
    public products_graph_data;
    public selected_product;
    public product_graph_categories = [];
    public user: any;

	ngOnInit() { 
        this.user = this.authService.getLoggedUser();
        this.user.is_agent = this.authService.isAgent();
        this.apiService.call('dashboard_stats/', 'GET').subscribe(stats => {
            this.lastest_products    = stats.response.recent_products;
            this.visits_graph_data   = stats.response.activitys.length   > 0 ? stats.response.activitys : false;
            this.products_graph_data = stats.response.products.length > 0 ? stats.response.products : false;
            this.products_graph_data.forEach(element => {
                this.product_graph_categories.push(element.name);
            });
        });
    }

    viewProduct(event) {
        this.router.navigate(['/app/products/edit/' + this.selected_product.id])
    }
    
}