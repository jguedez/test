import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service'
import { Observable, AsyncSubject } from 'rxjs/Rx';


@Injectable()
export class UsersService {

    constructor(private apiService: ApiService) { }

    all(body = {}) {
        return this.apiService.call('users', 'GET', body);
    }

    findById(id) {
        return this.apiService.call('users/' + id, 'GET');
    }

    create(user) {
        return this.apiService.call('users', 'POST', user);
    }

    update(user) {
        return this.apiService.call('users/' + user.id, 'PUT', user);
    }

    delete(id) {
        return this.apiService.call('users/' + id, 'DELETE');
    }
}
