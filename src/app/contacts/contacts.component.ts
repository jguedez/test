import { Component, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ContactsService } from "../shared/services/contacts.service";
import { GridComponent, GridDataResult, DataStateChangeEvent} from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';
import { Subscription} from 'rxjs/Subscription';

@Component({
	selector: 'contacts',
	templateUrl: './contacts.template.html'
})
export class ContactsPage implements OnDestroy, OnInit {
    public contacts = [];
    public loadingEnable = true;
    public filter = false;
    public gridData: GridDataResult;
    public contactSubscription: Subscription;
    public state = {
        skip: 0,
        take: 50
    };

    protected dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridData = process(this.contacts, this.state);
    }


	constructor(private contactsService: ContactsService, private router: Router, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.disableLoading();
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.filter = params['filter'];
            this.loadingEnable = true;
            this.contacts = [];
            this.getContacts(1);
        });
    }

	ngOnDestroy() { 
        this.loadingEnable = false;
        this.contactSubscription.unsubscribe();
    }

    onSelect(event) {
        this.disableLoading();
        this.router.navigate(['/app/contacts/edit/' + event.id]);
    }

    disableLoading() {
        this.loadingEnable = false;
    }

    getContacts(page) {
        this.contactSubscription=this.contactsService.all({page: page, filter: this.filter}).subscribe(contacts => 
        {
            page == 1 ? this.contacts = [] : null;

            contacts.response.data.forEach(record => {
                console.log(record);
                record.contacting_date = record.contacting_date ? new Date(record.contacting_date) : null;
            });
            
            this.contacts = contacts.response.data ? 
                                this.contacts.concat(contacts.response.data) : this.contacts;

            this.gridData = process(this.contacts, this.state);

            if(contacts.response.data.length > 0 && this.loadingEnable === true) { this.getContacts( page + 1) }
        });
    }
}
