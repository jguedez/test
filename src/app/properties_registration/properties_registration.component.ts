import { Component, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { PropertiesRegistrationService } from '../shared/services/properties_registration.service';
import { GridComponent, GridDataResult, DataStateChangeEvent} from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';

@Component({
	selector: 'properties_registration',
	templateUrl: './properties_registration.template.html'
})
export class PropertiesRegistrationPage implements OnDestroy, OnInit {
    public PRS = [];
    public loadingEnable = true;
    public filter = false;
    public gridData: GridDataResult;
    public state = {
        skip: 0,
        take: 50
    };

    protected dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridData = process(this.PRS, this.state);
    }

	constructor(private PRService: PropertiesRegistrationService, private router: Router, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.disableLoading();
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.filter = params['filter'];
            this.loadingEnable = true;
            this.PRS = [];
            this.getPRS(1);
        });
    }

	ngOnDestroy() { 
        this.loadingEnable = false;
    }

    onSelect(event) {
        this.disableLoading();
        this.router.navigate(['/app/properties_registration/edit/' + event.id]);
    }

    disableLoading() {
        this.loadingEnable = false;
    }

    getPRS(page) {
        this.PRService.all({page: page, filter: this.filter}).subscribe(PRS => 
        {
            page == 1 ? this.PRS = [] : null;
            
            this.PRS = PRS.response.data ? 
                                this.PRS.concat(PRS.response.data) : this.PRS;

            this.gridData = process(this.PRS, this.state);

            if(PRS.response.data.length > 0 && this.loadingEnable === true) { this.getPRS( page + 1) }
        });
    }
}
