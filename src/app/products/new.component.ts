import { Component, OnInit, EventEmitter } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ProductsService } from '../shared/services/products.service';

declare var swal: any;

@Component({
    selector: 'create',
    templateUrl: './new.template.html'
})
export class CreateProduct implements OnInit {

    public product = { links: {} };
    public processing = false;
    public productEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private location: Location, private productsService: ProductsService) { }

    ngOnInit() { 
        var productEventEmitter = this.productEventEmitter;
        setTimeout(function(){
            productEventEmitter.emit({});
        }, 500);
    }

    private create() {
        this.processing = true;
        this.productsService.create(this.product).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Producto creado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/products']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}