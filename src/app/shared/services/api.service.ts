import { Injectable } from '@angular/core';
import { Http, Response, Headers, Request, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable, AsyncSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

declare let swal: any;

@Injectable()
export class ApiService {

    constructor(private http: Http, private router: Router) { }

    //private apiEndPoint = 'http://api.skygroup.dev/';
    private apiEndPoint = 'http://skygroupapi.nemobile.net/';

    call(resource, method, body = {}) {
        let headers = { Authorization: "Bearer " + localStorage.getItem('token') };

        let requestOptionsArgs: RequestOptionsArgs = {
            url: this.apiEndPoint + resource,
            method: method,
            search: null,
            body: body,
            headers: new Headers(headers)
        }

        if( method == 'GET') { requestOptionsArgs.url += this.httpBuildQuery(body) }

        let requestOptions = new RequestOptions(requestOptionsArgs);

        return this.http.request(new Request(requestOptions))
            .map(response => this.responseInterceptor(response));
    }

    private responseInterceptor(response: any) {
        var parsed = response.json();

        switch (parsed.code) {
            case 101:
                localStorage.clear();
                this.router.navigate(['/login']);
                return parsed;

            case 102:
                swal("Ops", "No tienes permiso para realizar está acción", "error");
                return parsed;

            case 103:
                localStorage.clear();
                this.router.navigate(['/login'])
                return parsed;

            case 301:
                swal({
                    title: 'Ops',
                    text: this.parseErrors(parsed.response.errors),
                    html: true
                });
                return parsed;

            case 303:
                this.router.navigate(['/app/not-found']);
                return parsed;

            default:
                return parsed;
        }
    }

    private parseErrors(errors, str = '') {
        str += '<ul class="api-errors">';
        for (var error in errors) {
            for (var count = 0; count < errors[error].length; count++) {
                str += '<li>' + errors[error][count] + '</li>';
            }
        }
        str += '</ul>';
        return str;
    }

    private httpBuildQuery(body) {
        var str = [];
        for (var property in body)
            if (body.hasOwnProperty(property)) {
                str.push(encodeURIComponent(property) + "=" + encodeURIComponent(body[property]));
            }
        return '?' + str.join("&");
    }
}
