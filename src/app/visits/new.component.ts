import { Component, OnInit, EventEmitter } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { VisitsService } from '../shared/services/visits.service';

declare var swal: any;

@Component({
    selector: 'create',
    templateUrl: './new.template.html'
})
export class CreateVisit implements OnInit {

    public visit = {};
    public processing = false;
    public visitEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private location: Location, private visitsService: VisitsService) { }

    ngOnInit() { 
        var visitEventEmitter = this.visitEventEmitter;
        setTimeout(function(){
            visitEventEmitter.emit({});
        }, 500);
    }

    private create() {
        this.processing = true;
        this.visitsService.create(this.visit).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Actividad creada",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/activity']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}