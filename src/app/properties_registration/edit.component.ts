import { Component, OnInit, NgZone, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { Http, Headers } from "@angular/http";
import { md5 } from "../md5";
import { ActivatedRoute, Router } from '@angular/router';
import { PropertiesRegistrationService } from '../shared/services/properties_registration.service';

declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html'
})
export class EditPropertyRegistration implements OnInit {

    public PR = {};
    public processing = false;
    public PREventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private PRService: PropertiesRegistrationService) {
        this.route.params.subscribe(params => {
            this.PRService.findById(params['id']).subscribe(PR => {
                PR.response.lawyer_requirements = PR.response.lawyer_requirements ? PR.response.lawyer_requirements.split(";") : [];
                this.PR = PR.response;
                this.PREventEmitter.emit(PR);
            });
        });
    }

    ngOnInit() { }

    private save() {
        this.processing = true;
        this.PRService.update(this.PR).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Registro de venta o alquiler editado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/properties_registration']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }
}