import { Component, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ProductsService } from "../shared/services/products.service";
import {DataTableModule, SharedModule} from 'primeng/primeng';
import { GridComponent, GridDataResult, DataStateChangeEvent} from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';


@Component({
	selector: 'products',
	templateUrl: './products.template.html'
})
export class ProductsPage implements OnDestroy {
    public products = [];
    public loadingEnable = true;
    public filter = false;
    public fields = [];
    private queryFields = "";

    public gridData: GridDataResult;
    public state = {
        skip: 0,
        take: 50
    };

    protected dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridData = process(this.products, this.state);
    }

	constructor(private productsService: ProductsService, private router: Router, private activatedRoute: ActivatedRoute) {    }

	ngOnDestroy() { 
        this.loadingEnable = false;
    }

    ngOnInit() {
        this.disableLoading();
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.filter = params['filter'];
            this.getGridColumns(this.filter);
            this.loadingEnable = true;
            this.products = [];
            this.getProducts(1);
        });
    }

    onSelect(event) {
        this.disableLoading();
        this.router.navigate(['/app/products/edit/' + event.id]);
    }

    disableLoading() {
        this.loadingEnable = false;
    }

    getProducts(page) {
        this.productsService.all({page: page, filter: this.filter}).subscribe(products => 
        {
            page == 1 ? this.products = [] : null;

            products.response.data.forEach(record => {
                //console.log(record);
                record.updated_at = new Date(record.updated_at);
            });

            this.products = products.response.data ? 
                                this.products.concat(products.response.data) : this.products;

            this.gridData = process(this.products, this.state);

            if(products.response.data.length > 0 && this.loadingEnable === true) { this.getProducts( page + 1) }
        });
    }

    private getGridColumns(filter)
    {
        switch(filter)
        {
            case 'international_market':
                this.fields = [
                    {label: "Nombre", name: "name", type: "", width: "200"},
                    {label: "Código", name: "code", type: "", width: "150"},
                    {label: "Categoría", name: "category", type: "", width: "150"},
                    {label: "Urbanización", name: "urbanization", type: "", width: "200"},
                    {label: "Estado", name: "state", type: "", width: "200"},

                ];
            break;

            default:
                this.fields = [
                    {label: "Nombre", name: "name", type: "", width: "200"},
                    {label: "Código", name: "code", type: "", width: "150"},
                    {label: "Tipo", name: "offer_type", type: "", width: "150"},
                    {label: "Categoría", name: "category", type: "", width: "150"},
                    {label: "Urbanización", name: "urbanization", type: "", width: "200"},
                    {label: "Ciudad", name: "city", type: "", width: "200"},
                    {label: "Estado", name: "state", type: "", width: "200"},
                    {label: "Habitaciones", name: "bedrooms", type: "numeric", width: "150"},
                    {label: "Baños Princ.", name: "bathrooms", type: "numeric", width: "150"},
                    {label: "Puestos de Est.", name: "parking_places", type: "numeric", width: "150"},
                    {label: "Área/Const. M2", name: "area", type: "numeric", width: "150"},
                    /*{label: "Precio BS", name: "bsf_price", type: "", width: "190"},*/
                    
                    {label: "Área/Const. M2", name: "area", type: "numeric", width: "150"},
                    {label: "Punta Captadora", name: "captor_person_name", type: "", width: "200"},
                    {label: "Referencia", name: "usd_price", type: "numeric", width: "190"},
                ];
        }
    }
}
