import { Injectable, NgZone } from '@angular/core';
import { ApiService } from './api.service'
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

    constructor(private apiService: ApiService, private router: Router, private zone: NgZone) {}

    login(email, password) {
        return this.apiService.call('auth/login', 'POST', {
            email: email,
            password: password   
        });
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

    isLoggedIn(): boolean {
        return localStorage.getItem('token') != null && localStorage.getItem('user') != null;
    }

    getLoggedUser() {
        return JSON.parse(localStorage.getItem('user'));
    }

    getUserToken() {
        return localStorage.getItem('token');
    }
    
    isAdmin(): boolean {
        var user = this.getLoggedUser();
        return user.level == 'administrator'; 
    }

    isAgent(): boolean {
        var user = this.getLoggedUser();
        return user.level == 'agent'; 
    }
}
