import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service'
import { Observable, AsyncSubject } from 'rxjs/Rx';


@Injectable()
export class ProductsService {

    constructor(private apiService: ApiService) { }

    all(body = {}) {
        return this.apiService.call('products', 'GET', body);
    }

    findById(id) {
        return this.apiService.call('products/' + id, 'GET');
    }

    create(product) {
        return this.apiService.call('products', 'POST', this.normalizeProduct(product));
    }

    update(product) {
        return this.apiService.call('products/' + product.id, 'PUT', this.normalizeProduct(product));
    }

    private normalizeProduct(product)
    {
        var parsed = {};
        for(var field in product)
        {
            parsed[field] = product[field];
            if(product[field] instanceof Array && field != "links" )
            {
                parsed[field] = product[field].join(';')
            }
        }
        return parsed;
    }
}