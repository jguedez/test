import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';

import { AuthService } from 'app/shared/services/auth.service';
import { ApiService } from 'app/shared/services/api.service';
import { ContactsService } from 'app/shared/services/contacts.service';
import { ProductsService } from 'app/shared/services/products.service';
import { UsersService } from 'app/shared/services/users.service';
import { VisitsService } from 'app/shared/services/visits.service';
import { CommercialOfficeService } from 'app/shared/services/commercialOffices.service';
import { PropertiesRegistrationService } from 'app/shared/services/properties_registration.service';
import { Router, NavigationStart } from '@angular/router';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridModule } from '@progress/kendo-angular-grid';


import { AuthGuard } from 'app/shared/guards/auth.guard';
import { GuestGuard } from 'app/shared/guards/guest.guard';
import { AdminGuard } from 'app/shared/guards/admin.guard';

import { SearchPipe } from 'app/shared/pipes/search.pipe';
import { DatePipe } from '@angular/common';
import { PrecioPipe} from "app/shared/pipes/precio.pipe";

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { App } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InteralStateType } from './app.service';
import { AppConfig } from './app.config';
import { ErrorComponent } from './error/error.component';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  AppConfig,

  // Guards
  GuestGuard, AuthGuard, AdminGuard,
  // Services
  AuthService, ApiService, ContactsService, ProductsService, UsersService, VisitsService, 
  CommercialOfficeService, PropertiesRegistrationService, DatePipe,PrecioPipe,
];

type StoreType = {
  state: InteralStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ App ],
  declarations: [
    App,
    ErrorComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, { useHash: true }),
    BrowserAnimationsModule,
    GridModule
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {
  constructor(public appRef: ApplicationRef, public appState: AppState) {}

  hmrOnInit(store: StoreType) {
    if (!store || !store.state) return;
    console.log('HMR store', JSON.stringify(store, null, 2));
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues  = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}

