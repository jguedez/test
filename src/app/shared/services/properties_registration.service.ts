import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service'
import { Observable, AsyncSubject } from 'rxjs/Rx';


@Injectable()
export class PropertiesRegistrationService {

    constructor(private apiService: ApiService) { }

    all(body = {}) {
        return this.apiService.call('properties_registration', 'GET', body);
    }

    findById(id) {
        return this.apiService.call('properties_registration/' + id, 'GET');
    }

    create(pr) {
        return this.apiService.call('properties_registration', 'POST', this.normalizePR(pr));
    }

    update(pr) {
        return this.apiService.call('properties_registration/' + pr.id, 'PUT', this.normalizePR(pr));
    }

    private normalizePR(record)
    {
        var parsed = {};
        for(var field in record)
        {
            parsed[field] = record[field];
            if(record[field] instanceof Array)
            {
                parsed[field] = record[field].join(';')
            }
        }
        return parsed;
    }
}