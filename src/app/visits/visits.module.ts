import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VisitPage } from './visits.component';
import { CreateVisit } from './new.component';
import { EditVisit } from './edit.component';
import { SearchPipe } from './search.pipe';
import { VisitForm } from './form.component';
import { DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule } from 'primeng/primeng';
import { GridModule } from '@progress/kendo-angular-grid';


export const routes = [
	{ path: '', component: VisitPage, pathMatch: 'full' },
	{ path: 'create', component: CreateVisit, pathMatch: 'full' },
	{ path: 'edit/:id', component: EditVisit, pathMatch: 'full' },
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(routes), FormsModule, 
                DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, GridModule],
	declarations: [VisitPage, CreateVisit, EditVisit, SearchPipe, VisitForm]
})
export class VisitModule {
	static routes = routes;
}
