import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductsPage } from './products.component';
import { CreateProduct } from './new.component';
import { EditProduct } from './edit.component';
import { SearchPipe } from './search.pipe';
import { PrecioPipe } from './precio.pipe';
import { ProductForm } from './form.component';
import { DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, MultiSelectModule } from 'primeng/primeng';
import { GridModule } from '@progress/kendo-angular-grid';
//import { DecimalPipe } from '@angular/common';

export const routes = [
  { path: '', component: ProductsPage, pathMatch: 'full' },
  { path: 'create', component: CreateProduct, pathMatch: 'full' },
  { path: 'edit/:id', component: EditProduct, pathMatch: 'full' },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule,
  MultiSelectModule,DataTableModule, DropdownModule, DialogModule, MessagesModule, CalendarModule, GridModule],
  declarations: [ProductsPage, CreateProduct, EditProduct, ProductForm, SearchPipe,PrecioPipe]
})
export class ProductsModule {
  static routes = routes;
}