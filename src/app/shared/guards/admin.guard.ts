import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Rx';

@Injectable()

export class AdminGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var admin = this.authService.isAdmin();
        if(admin){
            return Observable.of(true);
        }
        this.router.navigate(['/dashboard']);
        return Observable.of(false);
    }
}