import { Component, OnInit, EventEmitter } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ContactsService } from '../shared/services/contacts.service';

declare var swal: any;

@Component({
    selector: 'create',
    templateUrl: './new.template.html'
})
export class CreateContact implements OnInit {

    public contact = {};
    public availability_checker = true;
    public firstSearch = false;
    public availabilityResponse = [];
    public processing = false;
    public contactEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router, private location: Location, private contactsService: ContactsService) { }

    ngOnInit() { 
        var contactEventEmitter = this.contactEventEmitter;
        setTimeout(function(){
            contactEventEmitter.emit({});
        }, 500);
    }

    private create() {
        this.processing = true;
        this.contactsService.create(this.contact).subscribe(data => {
            this.processing = false;
            if (data.code == 1) {
                var router = this.router;
                setTimeout(function () {
                    swal({
                        title: "Contacto creado",
                        text: "Operación completada",
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    setTimeout(function () {
                        router.navigate(['/app/contacts']);
                    }, 2000);
                }, 500);
            }
        });
    }

    private goBack() {
        this.location.back();
    }

    public availabilityCheckToggle() {
        this.availability_checker = !this.availability_checker;
        this.firstSearch = false; 
        this.availabilityResponse = [];
        this.processing = false;
    }

    public checkAvailability() {
        this.processing = true;
        this.firstSearch = true;
        this.contactsService.checkAvailability(this.contact).subscribe(response => {
            this.processing = false;
            if(response.response.availability){
                this.setAvailableMessage();
            
            // it can be undefined
            } else if (response.response.availability == false) {
                this.setUnavailableMessage(response.response.owner);
            }
        });
    }

    private setUnavailableMessage(owner) {
        owner = owner ? owner : {};
        this.availabilityResponse = [
            {
                severity: "error",
                summary: "Contacto tomado",
                detail: 
                "El contacto ya existe y es propiedad de: '" + owner.commercial_office + 
                "' asignado al asesor: '" + owner.name + " " + owner.last_name +
                "' - teléfono: " + owner.phone + " - correo: " + owner.email
            }
        ]
    }

    private setAvailableMessage() {
        this.availabilityResponse = [
            {
                severity: "success",
                summary: "Contacto disponible",
                detail: "El contacto está disponible para ser creado"
            }
        ]
    }
}